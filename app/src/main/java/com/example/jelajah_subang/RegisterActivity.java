package com.example.jelajah_subang;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.NetworkResponse;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

/**
 * A login screen that offers login via email/password.
 */
public class RegisterActivity extends MyCore {

    /**
     * Id to identity READ_CONTACTS permission request.
     */
    private static final int REQUEST_READ_CONTACTS = 0;

    /**
     * A dummy authentication store containing known user names and passwords.
     * TODO: remove after connecting to a real authentication system.
     */
    private String[] DUMMY_CREDENTIALS = new String[]{
            "foo@example.com:hello", "bar@example.com:world"
    };
    /**
     * Keep track of the login task to ensure we can cancel it if requested.
     */
    private String mAuthTask = null;

    // UI references.
    private EditText mEmailView;
    private EditText mPasswordView;
    private EditText mMobileView;
    private EditText mNamaView;
    private EditText mOTPView;
    TextView tvWelcome;
    Button mEmailSignInButton;

    TextInputLayout tilNama, tilPassword, tilEmail, tilMobileNumber, tilOtpNumber;
    TextView tvBtnOTP;

    private View mProgressView;
    private View mLoginFormView;

    String REGISTER_URL = "/registerProcess";
    private boolean registerIsSuccess = false;
    private boolean isSendOTP = false;
    int counter = 20;

    String email="not set";

    String id_kos="", nama_kos="", id_vendor="", alamat="", harga="", jumlah="", deskripsi="", ex_kasur="", ex_lemari="", ex_ac="", ex_kipas="", ex_wifi="", ex_tv="";
    String foto_kos="", slider_kos="";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        // Set up the login form.
        mEmailView = (EditText) findViewById(R.id.email);
        mMobileView = (EditText) findViewById(R.id.mobile_number);
        mNamaView = (EditText) findViewById((R.id.nama));
        mOTPView = (EditText) findViewById((R.id.otp_number));
        tvBtnOTP = (TextView) findViewById(R.id.tvBtnOTP);
        tvBtnOTP.setVisibility(View.GONE);
        tvBtnOTP.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                isSendOTP = true;
                REGISTER_URL = "/getOTPNumber";
                attemptRegister();
            }
        });
        tvWelcome = (TextView) findViewById(R.id.tvWelcome);
        tvWelcome.setText("Silahkan isi data diri Anda");

        tilNama = (TextInputLayout) findViewById(R.id.tilNama);
        tilEmail = (TextInputLayout) findViewById(R.id.tilEmail);
        tilPassword = (TextInputLayout) findViewById(R.id.tilPassword);
        tilMobileNumber = (TextInputLayout) findViewById(R.id.tilMobileNumber);
        tilOtpNumber = (TextInputLayout) findViewById(R.id.tilOtpNumber);
        tilOtpNumber.setVisibility(View.GONE);


        // GET BUNDLE EXTRA DATA from previous activity
        getExtrasData();

        mPasswordView = (EditText) findViewById(R.id.password);
        mPasswordView.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int id, KeyEvent keyEvent) {
                if (id == EditorInfo.IME_ACTION_DONE || id == EditorInfo.IME_NULL) {
                    //attemptRegister();
                    return true;
                }
                return false;
            }
        });

        mEmailSignInButton = (Button) findViewById(R.id.email_sign_in_button);
        mEmailSignInButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                isSendOTP = false;
                REGISTER_URL = "/registerProcess";
                attemptRegister();
            }
        });

        mLoginFormView = findViewById(R.id.login_form);
        mProgressView = findViewById(R.id.login_progress);
    }

    public void getExtrasData() {
        Bundle bundle = getIntent().getExtras();

        email = bundle.getString("email");

        id_kos = bundle.getString("id_kos");
        nama_kos = bundle.getString("nama_kos");
        foto_kos = bundle.getString("foto_kos");
        slider_kos = bundle.getString("slider_kos");
        id_vendor = bundle.getString("id_vendor");
        alamat = bundle.getString("alamat");
        harga = bundle.getString("harga");
        jumlah = bundle.getString("jumlah");
        deskripsi = bundle.getString("deskripsi");

        //mEmailView.setText(email);
    }

    public void setOTPCounter() {
        tvBtnOTP.setVisibility(View.GONE);
        int counterMiliseconds = counter*1000;
        new CountDownTimer(counterMiliseconds,1000) {
            @Override
            public void onTick(long millisUntilFinished) {
                counter--;
            }
            @Override
            public void onFinish() {
                //counttime.setText("Finished");
                tvBtnOTP.setVisibility(View.VISIBLE);
            }
        }.start();
    }

    /**
     * Attempts to sign in or register the account specified by the login form.
     * If there are form errors (invalid email, missing fields, etc.), the
     * errors are presented and no actual login attempt is made.
     */
    private void attemptRegister() {
        if (mAuthTask != null) {
            return;
        }

        mEmailView.setError(null);
        mPasswordView.setError(null);
        mMobileView.setError(null);
        mNamaView.setError(null);

        // Store values at the time of the login attempt.
        String email = mEmailView.getText().toString();
        String password = mPasswordView.getText().toString();
        String mobile_number = mMobileView.getText().toString();
        String nama = mNamaView.getText().toString();
        String otp_number = mOTPView.getText().toString();

        boolean cancel = false;
        View focusView = null;

        // Check for a valid password, if the user entered one.
        if (!TextUtils.isEmpty(password) && !isPasswordValid(password) && !registerIsSuccess) {
            mPasswordView.setError("Password terlalu pendek");
            focusView = mPasswordView;
            cancel = true;
        }

        // Check for a valid mobile number, if the user entered one.
        if (!TextUtils.isEmpty(mobile_number) && !isMobileNumberValid(mobile_number) && !registerIsSuccess) {
            mMobileView.setError("Nomor Hp terlalu pendek");
            focusView = mMobileView;
            cancel = true;
        }

        // Check for a valid nama, if the user entered one.
        if (TextUtils.isEmpty(nama) && !registerIsSuccess) {
            mNamaView.setError("Nama harus diisi");
            focusView = mNamaView;
            cancel = true;
        }

        // check otp number
        if (!isOTPValid(otp_number) && registerIsSuccess && isSendOTP) {
            //Log.d("otp process", String.valueOf(registerIsSuccess) + "--" +String.valueOf(isOTPValid(otp_number)));
            mOTPView.setError("Kode OTP salah");
            focusView = mOTPView;
            cancel = true;
        }

        // Check for a valid email address.
        if (TextUtils.isEmpty(email) && !registerIsSuccess) {
            mEmailView.setError("Email harus diisi");
            focusView = mEmailView;
            cancel = true;
        } else if (!isEmailValid(email) && !registerIsSuccess) {
            mEmailView.setError(getString(R.string.error_invalid_email));
            focusView = mEmailView;
            cancel = true;
        }

        if (cancel) {
            // There was an error; don't attempt login and focus the first
            // form field with an error.
            focusView.requestFocus();
        } else {
            // Show a progress spinner, and kick off a background task to
            // perform the user login attempt.
            showProgress(true);
            registerProcess(email, password, mobile_number, nama, otp_number);
        }
    }

    private boolean isEmailValid(String email) {
        //TODO: Replace this with your own logic
        return email.contains("@");
    }

    private boolean isPasswordValid(String password) {
        //TODO: Replace this with your own logic
        return password.length() > 4;
    }

    private boolean isMobileNumberValid(String password) {
        //TODO: Replace this with your own logic
        return password.length() > 10;
    }

    private boolean isOTPValid(String otp_number) {
        //TODO: Replace this with your own logic
        return otp_number.length() == 6;
    }

    /**
     * Shows the progress UI and hides the login form.
     */
    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
    private void showProgress(final boolean show) {
        // On Honeycomb MR2 we have the ViewPropertyAnimator APIs, which allow
        // for very easy animations. If available, use these APIs to fade-in
        // the progress spinner.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
            int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);

            mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
            mLoginFormView.animate().setDuration(shortAnimTime).alpha(
                    show ? 0 : 1).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
                }
            });

            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mProgressView.animate().setDuration(shortAnimTime).alpha(
                    show ? 1 : 0).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
                }
            });
        } else {
            // The ViewPropertyAnimator APIs are not available, so simply show
            // and hide the relevant UI components.
            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
        }
    }



    public void registerProcess(final String email, final String password, final String mobile_number, final String nama, final String otp_number) {
        showProgress(true);
        StringRequest sReq = new StringRequest(com.android.volley.Request.Method.POST,
                Public.SERVER_URL + REGISTER_URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        // close dialog
                        Log.d("URL API", Public.SERVER_URL + REGISTER_URL);
                        Log.d("response", String.valueOf(response.isEmpty()));
                        if(!response.isEmpty()) {
                            try {

                                JSONObject rowData = new JSONObject(response);

                                Log.d("response object", rowData.toString());

                                    //Toast.makeText(RegisterActivity.this, rowData.getString("otp_number"), Toast.LENGTH_SHORT).show();
                                    if (!registerIsSuccess) {
                                        registerIsSuccess = true;

                                        // OTP CODE (temporary not used)
                                        tilOtpNumber.setVisibility(View.VISIBLE);
                                        tilEmail.setVisibility(View.GONE);
                                        tilNama.setVisibility(View.GONE);
                                        tilPassword.setVisibility(View.GONE);
                                        tilMobileNumber.setVisibility(View.GONE);
                                        tvBtnOTP.setVisibility(View.VISIBLE);
                                        mEmailSignInButton.setText("Send");
                                        tvWelcome.setText("Masukkan Kode OTP");

                                        //goToCheckout();
                                    } else {
                                        //finish();
                                        goToCheckout();
                                    }

                            } catch (Exception e) {
                                Log.d("error get data", e.toString());
                                //progressDialog.dismiss();
                            }
                        } else if (isSendOTP && response.isEmpty()) {
                            setOTPCounter();
                            Toast.makeText(RegisterActivity.this, "Kode OTP telah dikirimkan kembali melalui SMS", Toast.LENGTH_LONG).show();
                        } else {
                            // if using otp validation
                            mOTPView.setError("Kode OTP Salah");
                            mOTPView.requestFocus();

                            // if not using otp validation
                            /*mEmailView.setError("Email sudah terdaftar, silahkan login");
                            mEmailView.requestFocus();*/
                        }

                        showProgress(false);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        String jsonError = "Connection timeout";
                        NetworkResponse networkResponse = error.networkResponse;
                        if (networkResponse != null && networkResponse.data != null) {
                            jsonError = new String(networkResponse.data);
                            // Print Error!
                        }
                        Toast.makeText(RegisterActivity.this, error.toString(), Toast.LENGTH_SHORT).show();
                        showProgress(false);
                    }
                }) {
            public Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("email", email);
                params.put("password", password);
                params.put("mobile_number", mobile_number);
                params.put("nama", nama);
                params.put("register_is_success", Boolean.toString(registerIsSuccess));
                params.put("otp_number", otp_number);

                Log.d("params", params.toString());
                return params;
            }
        };
        Volley.newRequestQueue(RegisterActivity.this).add(sReq);
    }

    public void goToCheckout() {
        String email = mEmailView.getText().toString();
        Intent intent = new Intent(RegisterActivity.this, LOGIN_REDIRECT_ACTIVITY);

        intent.putExtra("email", email);

        intent.putExtra("id_kos", id_kos);
        intent.putExtra("nama_kos", nama_kos);
        intent.putExtra("foto_kos", foto_kos);
        intent.putExtra("slider_kos", slider_kos);
        intent.putExtra("id_vendor", id_vendor);
        intent.putExtra("alamat", alamat);
        intent.putExtra("harga", harga);
        intent.putExtra("jumlah", jumlah);
        intent.putExtra("deskripsi", deskripsi);

        startActivity(intent);
    }
}

