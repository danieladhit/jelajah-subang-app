package com.example.jelajah_subang;

import android.content.Context;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.jelajah_subang.object.Kos;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import static com.example.jelajah_subang.Public.BASE_URL;

/**
 * Created by Jhedhot 2018.
 */

public abstract class RecyclePlace extends RecyclerView.Adapter<RecyclePlace.MyViewHolder> implements Filterable {

    List<Kos> datas;
    Context context;
    boolean edit;
    List<Kos> searchList;

    public RecyclePlace(Context context, List<Kos> param, boolean edit){
        this.context = context;
        this.datas = param;
        this.edit = edit;
        this.searchList = param;
    }

    int blankImage = R.drawable.img_blank_subang;
    int notFoundImage = R.drawable.img_not_found;

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.adapter_recycle_kos, parent, false);
        itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
        return new MyViewHolder(itemView);
    }

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        final Kos data = searchList.get(position);

        String foto = BASE_URL + data.getFoto_kos();
        Picasso.get().load(foto).placeholder(blankImage).error(notFoundImage).fit().centerCrop().into(holder.imageView);
        Log.d("load", String.valueOf(position));
        holder.tv1.setText(data.getNama_kos());
        holder.tv2.setText(data.getHarga());
        holder.tv3.setText(data.getAlamat());
        int jarak = (int) Math.ceil(Double.parseDouble(data.getJarak()));
        holder.tvJarak.setText(String.valueOf(jarak) + " km");

        holder.layParent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setOnClickListener(data);
            }
        });

    }

    @Override
    public int getItemCount() {
        return searchList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public ImageView imageView;
        public TextView tv1, tv2, tv3, tvJarak;
        public LinearLayout layParent;

        public MyViewHolder(View view) {
            super(view);
            imageView = (ImageView) view.findViewById(R.id.imageView);
            tv1 = (TextView) view.findViewById(R.id.tv1);
            tv2 = (TextView) view.findViewById(R.id.tv2);
            tv3 = (TextView) view.findViewById(R.id.tv3);
            tvJarak = (TextView) view.findViewById(R.id.tvJarak);
            layParent = (LinearLayout) view.findViewById(R.id.layParent);
        }
    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                String charString = charSequence.toString();
                if (charString.isEmpty()) {
                    searchList = datas;
                } else {
                    List<Kos> filteredList = new ArrayList<>();
                    for (Kos row : datas) {
                        // name match condition. this might differ depending on your requirement
                        // here we are looking for name or phone number match
                        if (row.getNama_kos().toLowerCase().contains(charString.toLowerCase()) || row.getAlamat().toLowerCase().contains(charString.toLowerCase()) || row.getHarga().toLowerCase().contains(charString.toLowerCase())) {
                            filteredList.add(row);
                        }
                    }

                    searchList = filteredList;
                }

                FilterResults filterResults = new FilterResults();
                filterResults.values = searchList;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                searchList = (ArrayList<Kos>) filterResults.values;
                notifyDataSetChanged();
            }
        };
    }

    public abstract void setOnClickListener(Kos store);

}
