package com.example.jelajah_subang;

import android.content.Context;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.jelajah_subang.object.Kos;
import com.squareup.picasso.Picasso;

import java.util.List;

import static com.example.jelajah_subang.Public.BASE_URL;

/**
 * Created by Jhedhot 2018.
 */

public abstract class RecycleHistory extends RecyclerView.Adapter<RecycleHistory.MyViewHolder> {

    List<Kos> datas;
    Context context;
    boolean edit;
    public RecycleHistory(Context context, List<Kos> param, boolean edit){
        this.context = context;
        this.datas = param;
        this.edit = edit;
    }

    int blankImage = R.drawable.img_blank_subang;
    int notFoundImage = R.drawable.img_not_found;

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.adapter_recycle_history, parent, false);
        itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
        return new MyViewHolder(itemView);
    }

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        final Kos data = datas.get(position);

        String foto = BASE_URL + data.getFoto_kos();
        Picasso.get().load(foto).placeholder(blankImage).error(notFoundImage).fit().centerCrop().into(holder.imageView);
        Log.d("foto", data.getFoto_kos());
        holder.tv1.setText(data.getWaktu_order());
        holder.tv2.setText(data.getNama_kos());
        holder.tv3.setText(data.getPeriode());
        holder.tv4.setText("Rp. " + data.getHarga());
        holder.tv5.setText(data.getStatus_order());

        holder.layParent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setOnClickListener(data);
            }
        });

    }

    @Override
    public int getItemCount() {
        return this.datas.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public ImageView imageView;
        public TextView tv1, tv2, tv3, tv4, tv5;
        public LinearLayout layParent;

        public MyViewHolder(View view) {
            super(view);
            imageView = (ImageView) view.findViewById(R.id.imageView);
            tv1 = (TextView) view.findViewById(R.id.tv1);
            tv2 = (TextView) view.findViewById(R.id.tv2);
            tv3 = (TextView) view.findViewById(R.id.tv3);
            tv4 = (TextView) view.findViewById(R.id.tv4);
            tv5 = (TextView) view.findViewById(R.id.tv5);
            layParent = (LinearLayout) view.findViewById(R.id.layParent);
        }
    }

    public abstract void setOnClickListener(Kos store);

}
