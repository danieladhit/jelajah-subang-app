package com.example.jelajah_subang;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.android.volley.NetworkResponse;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.jelajah_subang.object.Kos;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class HistoryActivity extends MyCore {
    List<Kos> listData;
    RecycleHistory adapterList;
    ProgressDialog progressDialog;
    RecyclerView recycleList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_history);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        //Toast.makeText(HistoryActivity.this, "Email Session : " + SP_EMAIL, Toast.LENGTH_SHORT).show();

        listData = new ArrayList<Kos>();
        adapterList = new RecycleHistory(HistoryActivity.this, listData, false) {
            @Override
            public void setOnClickListener(Kos row) {
                //Toast.makeText(MainActivity.this, "Selected kos : " + row.getNama_kos(), Toast.LENGTH_SHORT).show();
                if(row.getStatus_order().equals("Belum Bayar") || row.getStatus_order().equals("Konfirmasi Pembayaran Ditolak")) {
                    showDetailKos(row);
                } else if(row.getStatus_order().equals("Sudah Lunas")) {
                    Toast.makeText(HistoryActivity.this, "Order ini sudah lunas", Toast.LENGTH_SHORT).show();
                } else if(row.getStatus_order().equals("Sudah Konfirmasi")) {
                    Toast.makeText(HistoryActivity.this, "Order ini sudah di konfirmasi", Toast.LENGTH_SHORT).show();
                }

            }
        };

        recycleList = (RecyclerView) findViewById(R.id.recycleList);
        recycleList.setAdapter(adapterList);
        recycleList.setLayoutManager(new LinearLayoutManager(HistoryActivity.this));
        recycleList.setItemAnimator(new DefaultItemAnimator());

        refreshData();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);

        // Hidden logout menu
        if(!isLogin()) {
            menu.getItem(2).setVisible(false); // Order History menu
            menu.getItem(3).setVisible(false); // logout menu
        } else {
            menu.getItem(2).setVisible(true); // Order History menu
            menu.getItem(3).setVisible(true); // logout menu
        }

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        return super.onOptionsItemSelected(item);
    }

    public void refreshData() {
        listData.clear();
        getServerData();
    }

    public void getServerData() {
        StringRequest sReq = new StringRequest(com.android.volley.Request.Method.POST,
                Public.SERVER_URL + "/getOrderHistory",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        // close dialog
                        Log.d("response", response);
                        try {
                            JSONArray responses = new JSONArray(response);
                            Log.d("response object", response.toString());
                            final int numberOfItemsInResp = responses.length();
                            if(numberOfItemsInResp > 0){
                                for(int i = 0; i < numberOfItemsInResp; i++) {
                                    JSONObject data = responses.getJSONObject(i);

                                    Kos row = new Kos();

                                    // KOS DATA
                                    row.setId_kos(data.getString("id"));
                                    row.setNama_kos(data.getString("nama_kos"));
                                    row.setId_vendor(data.getString("id_vendor"));
                                    row.setAlamat(data.getString("alamat"));
                                    row.setHarga(data.getString("harga"));
                                    row.setJumlah(data.getString("jumlah"));
                                    row.setDeskripsi(data.getString("deskripsi"));
                                    row.setFoto_kos(data.getString("foto_kos"));
                                    row.setEx_ac(data.getString("ex_ac"));
                                    row.setEx_kasur(data.getString("ex_kasur"));
                                    row.setEx_kipas(data.getString("ex_kipas"));
                                    row.setEx_lemari(data.getString("ex_lemari"));
                                    row.setEx_tv(data.getString("ex_tv"));
                                    row.setEx_wifi(data.getString("ex_wifi"));

                                    // ORDER DATA
                                    row.setOrder_id(data.getString("order_id"));
                                    row.setPeriode(data.getString("periode"));
                                    row.setWaktu_order(data.getString("waktu_order"));
                                    row.setHarga_sewa(data.getString("harga_sewa"));
                                    row.setNo_rekening(data.getString("no_rekening"));
                                    row.setJumlah_transfer(data.getString("jumlah_transfer"));
                                    row.setBukti_pembayaran(data.getString("bukti_pembayaran"));
                                    row.setStatus_order(data.getString("status_order"));

                                    listData.add(row);
                                }

                                adapterList.notifyDataSetChanged();

                            } else {
                                // get response data and convert to array
                                JSONObject data = responses.getJSONObject(0);

                                AlertDialog.Builder error = new AlertDialog.Builder(HistoryActivity.this);
                                error.setTitle("Connection Timeouttt");
                                error.setMessage(data.getString("message"));
                                error.create();
                                error.show();
                            }
                            //progressDialog.dismiss();
                        } catch (Exception e){
                            Log.d("error get schedule", e.toString());
                            //progressDialog.dismiss();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        String jsonError = "Connection timeout";
                        NetworkResponse networkResponse = error.networkResponse;
                        if (networkResponse != null && networkResponse.data != null) {
                            jsonError = new String(networkResponse.data);
                            // Print Error!
                        }
                        Toast.makeText(HistoryActivity.this, error.toString(), Toast.LENGTH_SHORT).show();
                        //progressDialog.dismiss();
                    }
                }) {
            public Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("email", SP_EMAIL);

                Log.d("params", params.toString());
                return params;
            }
        };
        Volley.newRequestQueue(HistoryActivity.this).add(sReq);
    }

    /** Called when the user taps the Send button */
    public void showDetailKos(Kos row) {
        Intent intent = new Intent(this, CheckoutActivity.class);

        intent.putExtra("email", SP_EMAIL);
        intent.putExtra("waktu_order", row.getWaktu_order());
        intent.putExtra("order_id", row.getOrder_id());

        intent.putExtra("id_kos", row.getId_kos());
        intent.putExtra("nama_kos", row.getNama_kos());
        intent.putExtra("foto_kos", row.getFoto_kos());
        intent.putExtra("slider_kos", row.getSlider_kos());
        intent.putExtra("id_vendor", row.getId_vendor());
        intent.putExtra("alamat", row.getAlamat());
        intent.putExtra("harga", "Rp. " + row.getHarga());
        intent.putExtra("jumlah", row.getJumlah());
        intent.putExtra("deskripsi", row.getDeskripsi());

        intent.putExtra("ex_ac", row.getEx_ac());
        intent.putExtra("ex_kasur", row.getEx_kasur());
        intent.putExtra("ex_kipas", row.getEx_kipas());
        intent.putExtra("ex_lemari", row.getEx_lemari());
        intent.putExtra("ex_tv", row.getEx_tv());
        intent.putExtra("ex_wifi", row.getEx_wifi());

        startActivity(intent);
    }

}
