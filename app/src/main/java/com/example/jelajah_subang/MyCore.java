package com.example.jelajah_subang;

import android.Manifest;
import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Build;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

public class MyCore extends AppCompatActivity {

    SharedPrefManager sharedPrefManager;
    public Class LOGIN_REDIRECT_ACTIVITY = MainActivity.class;
    public final String ACTIVITY_HOME = "home";
    public final String ACTIVITY_CHECKOUT = "checkout";
    public final String ACTIVITY_HISTORY = "orderHistory";

    String SP_NAMA="", SP_EMAIL="", SP_TELP="", SP_ALAMAT="";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        sharedPrefManager = new SharedPrefManager(this);

        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 1);
        }
        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.CAMERA}, 2);
        }
        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_COARSE_LOCATION}, 3);
        }
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 4);
        }

        if (isLogin()){
            SP_NAMA = sharedPrefManager.getSPNama();
            SP_EMAIL = sharedPrefManager.getSPEmail();
            SP_ALAMAT = sharedPrefManager.getSPAlamat();
            SP_TELP = sharedPrefManager.getSPTelp();
        }

        // set login redirect activity
        getLoginRedirectActivity();
    }

    public Boolean isLogin() {
        return sharedPrefManager.getSPIsLogin();
    }

    public void setLogin(String email) {
        sharedPrefManager.saveSPBoolean(SharedPrefManager.SP_IS_LOGIN, true);
//        sharedPrefManager.saveSPString(SharedPrefManager.SP_NAMA, nama);
        sharedPrefManager.saveSPString(SharedPrefManager.SP_EMAIL, email);
//        sharedPrefManager.saveSPString(SharedPrefManager.SP_ALAMAT, alamat);
//        sharedPrefManager.saveSPString(SharedPrefManager.SP_TELP, telp);
    }

    public void  setLogout() {
        sharedPrefManager.saveSPBoolean(SharedPrefManager.SP_IS_LOGIN, false);
        sharedPrefManager.saveSPString(SharedPrefManager.SP_NAMA, "");
        sharedPrefManager.saveSPString(SharedPrefManager.SP_EMAIL, "");
        sharedPrefManager.saveSPString(SharedPrefManager.SP_ALAMAT, "");
        sharedPrefManager.saveSPString(SharedPrefManager.SP_TELP, "");
    }

    public void setLoginRedirectActivity(String stringActivity) {
        sharedPrefManager.saveSPString("LOGIN_REDIRECT_STRING", stringActivity);
    }

    public void getLoginRedirectActivity(){
        String activityName = sharedPrefManager.getSPLoginRedirect();
        if (activityName.isEmpty()) {
            setLoginRedirectActivity(ACTIVITY_HOME);
            LOGIN_REDIRECT_ACTIVITY = MainActivity.class;
        } else {
            switch (activityName) {
                case ACTIVITY_CHECKOUT:
                    LOGIN_REDIRECT_ACTIVITY = CheckoutActivity.class;
                    break;
                case ACTIVITY_HISTORY:
                    LOGIN_REDIRECT_ACTIVITY = HistoryActivity.class;
                    break;
                case ACTIVITY_HOME:
                    LOGIN_REDIRECT_ACTIVITY = MainActivity.class;
                    break;
            }
        }
    }

    /**
     * Shows the progress UI and hides the login form.
     */
    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
    private void showProgress(final boolean show, final View viewToHide, final View mProgressView) {
        // On Honeycomb MR2 we have the ViewPropertyAnimator APIs, which allow
        // for very easy animations. If available, use these APIs to fade-in
        // the progress spinner.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
            int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);

            viewToHide.setVisibility(show ? View.GONE : View.VISIBLE);
            viewToHide.animate().setDuration(shortAnimTime).alpha(
                    show ? 0 : 1).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    viewToHide.setVisibility(show ? View.GONE : View.VISIBLE);
                }
            });

            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mProgressView.animate().setDuration(shortAnimTime).alpha(
                    show ? 1 : 0).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
                }
            });
        } else {
            // The ViewPropertyAnimator APIs are not available, so simply show
            // and hide the relevant UI components.
            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            viewToHide.setVisibility(show ? View.GONE : View.VISIBLE);
        }
    }

}
