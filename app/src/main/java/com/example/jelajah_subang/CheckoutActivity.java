package com.example.jelajah_subang;

import android.Manifest;
import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Build;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.NetworkResponse;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.util.HashMap;
import java.util.Map;

public class CheckoutActivity extends MyCore implements View.OnClickListener {

    EditText etAccountNumber, etAmount;
    ImageView imageSelect, image;
    TextView imageText;
    LinearLayout imageContainer;
    Button btnKonfirmasi;
    String getPath="";

    private View mProgressView;

    private static final String IMAGE_DIRECTORY_NAME = "Carikos/Bukti_Transfer";
    private static String PATHIMAGES = "";
    private static final int CAMERA_CAPTURE_IMAGE_REQUEST_CODE = 100;
    private static final int CAMERA_SELECT_IMAGE_REQUEST_CODE = 200;

    String email="",id_kos="", nama_kos="", id_vendor="", alamat="", harga="", jumlah="", deskripsi="", ex_kasur="", ex_lemari="", ex_ac="", ex_kipas="", ex_wifi="", ex_tv="";
    String foto_kos="", slider_kos="", waktu_order="", order_id="";
    String account_number="", amount="", image64="";

    String CHECKOUT_URL = "/checkoutProcess";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_checkout);

        getExtrasData();

        etAccountNumber = (EditText) findViewById(R.id.nama);
        etAmount = (EditText) findViewById(R.id.password);

        image = (ImageView) findViewById(R.id.image);
        imageText = (TextView) findViewById(R.id.imageText);
        imageContainer = (LinearLayout) findViewById(R.id.imageContainer);

        mProgressView = findViewById(R.id.login_progress);

        btnKonfirmasi = (Button) findViewById(R.id.btnKonfirmasi);

        //imageSelect.setOnClickListener(this);
        image.setOnClickListener(this);

        btnKonfirmasi.setOnClickListener(this);
    }

    public void getExtrasData() {
        Bundle bundle = getIntent().getExtras();

        email = bundle.getString("email");

        id_kos = bundle.getString("id_kos");
        nama_kos = bundle.getString("nama_kos");
        foto_kos = bundle.getString("foto_kos");
        slider_kos = bundle.getString("slider_kos");
        id_vendor = bundle.getString("id_vendor");
        alamat = bundle.getString("alamat");
        harga = bundle.getString("harga");
        jumlah = bundle.getString("jumlah");
        deskripsi = bundle.getString("deskripsi");

        try {
            order_id = bundle.getString("order_id");
            waktu_order = bundle.getString("waktu_order");

            if (!order_id.equals("")) {
                CHECKOUT_URL = "/konfirmasiPembayaran";
            }
        } catch (Exception e) {
            Log.d("waktu order", e.getMessage().toString());
        }

        //Toast.makeText(CheckoutActivity.this, email + "-" + nama_kos + "-" + id_kos , Toast.LENGTH_SHORT).show();
    }

    /**
     * Shows the progress UI and hides the login form.
     */
    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
    private void showProgress(final boolean show) {
        // On Honeycomb MR2 we have the ViewPropertyAnimator APIs, which allow
        // for very easy animations. If available, use these APIs to fade-in
        // the progress spinner.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
            int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);

            /*mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
            mLoginFormView.animate().setDuration(shortAnimTime).alpha(
                    show ? 0 : 1).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
                }
            });*/

            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mProgressView.animate().setDuration(shortAnimTime).alpha(
                    show ? 1 : 0).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
                }
            });
        } else {
            // The ViewPropertyAnimator APIs are not available, so simply show
            // and hide the relevant UI components.
            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            //mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.image:
                dialogImage(image);
                break;

            case R.id.btnKonfirmasi:
                android.app.AlertDialog.Builder exit = new android.app.AlertDialog.Builder(this);
                exit.setTitle("Konfirmasi Order");
                exit.setMessage("Anda akan melakukan konfirmasi pembayaran ?");
                exit.setPositiveButton("Ya", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        konfirmasiSekarang(dialogInterface);
                    }
                });
                exit.setNegativeButton("Nanti Saja", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        konfirmasiNanti();
                    }
                });
                exit.create();
                exit.show();
                break;
        }
    }

    public void konfirmasiSekarang(DialogInterface dialogInterface) {
        boolean cancel = false;
        View focusView = null;

        account_number = etAccountNumber.getText().toString();
        amount = etAmount.getText().toString();

        if (TextUtils.isEmpty(account_number)) {
            etAccountNumber.setError("Nomor rekening harus diisi !!");
            focusView = etAccountNumber;
            cancel = true;
        }

        if (TextUtils.isEmpty(amount)) {
            etAmount.setError("Jumlah transfer harus diisi");
            focusView = etAmount;
            cancel = true;
        }

        if (TextUtils.isEmpty(image64)) {
            Toast.makeText(CheckoutActivity.this, "Harap foto bukti pembayaran terlebih dahulu", Toast.LENGTH_SHORT).show();
            focusView = image;
            cancel = true;
        }

        if (cancel) {
            // There was an error; don't attempt login and focus the first
            // form field with an error.
            focusView.requestFocus();
            dialogInterface.dismiss();
        } else {
            // Show a progress spinner, and kick off a background task to
            // perform the user login attempt.
            showProgress(true);
            saveOrder();
        }
    }

    public void konfirmasiNanti() {
        account_number = "";
        amount = "";
        image64 = "";
        saveOrder();
    }

    public void saveOrder() {
        showProgress(true);
        StringRequest sReq = new StringRequest(com.android.volley.Request.Method.POST,
                Public.SERVER_URL + CHECKOUT_URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        // close dialog
                        Log.d("URL API", Public.SERVER_URL + CHECKOUT_URL);
                        Log.d("response", response);
                        if(!response.isEmpty()) {
                            try {

                                JSONObject rowData = new JSONObject(response);

                                Log.d("response object", rowData.toString());

                                Toast.makeText(CheckoutActivity.this, "Checkout Berhasilll !!!", Toast.LENGTH_SHORT).show();

                                goToOrderHistory();

                            } catch (Exception e) {
                                Log.d("error get data", e.toString());
                                //progressDialog.dismiss();
                            }
                        } else {
                            Toast.makeText(CheckoutActivity.this, "Checkout Gagal !!!", Toast.LENGTH_SHORT).show();
                        }
                        showProgress(false);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        String jsonError = "Connection timeout";
                        NetworkResponse networkResponse = error.networkResponse;
                        if (networkResponse != null && networkResponse.data != null) {
                            jsonError = new String(networkResponse.data);
                            // Print Error!
                        }
                        Toast.makeText(CheckoutActivity.this, error.toString(), Toast.LENGTH_SHORT).show();
                        showProgress(false);
                    }
                }) {
            public Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("email", email);
                params.put("id_kos", id_kos);
                params.put("id_vendor", id_vendor);
                params.put("account_number", account_number);
                params.put("amount", amount);
                params.put("bukti_pembayaran", image64);

                // khusus checkout dari Order Histori
                if(CHECKOUT_URL == "/konfirmasiPembayaran") {
                    params.put("order_id", order_id);
                    params.put("waktu_order", waktu_order);
                } else {
                    Log.d("Null Params", "Order ID and Waktu Order kosong");
                }

                Log.d("params", params.toString());
                return params;
            }
        };
        Volley.newRequestQueue(CheckoutActivity.this).add(sReq);
    }

    public void goToOrderHistory() {
        Intent intent = new Intent(CheckoutActivity.this, HistoryActivity.class);

        intent.putExtra("email", email);

        startActivity(intent);
    }

    public void dialogImage(ImageView target){
        imageSelect = target;
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        LayoutInflater inflater = this.getLayoutInflater();

        View layout = inflater.inflate(R.layout.dialog_choose_image, null);

        ImageView imageCamera = layout.findViewById(R.id.imageCamera);
        ImageView imageGallery = layout.findViewById(R.id.imageGallery);

        builder.setView(layout);

        final AlertDialog dialog = builder.create();
        dialog.show();

        if (ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA)
                != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.CAMERA},
                    0);
        }

        imageCamera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                myOpenCamera();
                dialog.dismiss();
            }
        });

        imageGallery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openGallery();
                dialog.dismiss();
            }
        });
    }

    public void myOpenCamera(){
        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.WRITE_EXTERNAL_STORAGE)!= PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 1);
        }

        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(intent, CAMERA_CAPTURE_IMAGE_REQUEST_CODE);

    }

    public void openGallery(){
        Intent intent = new Intent(Intent.ACTION_PICK,
                android.provider.MediaStore.Images.Media.INTERNAL_CONTENT_URI);
        startActivityForResult(intent, CAMERA_SELECT_IMAGE_REQUEST_CODE);
    }


    @Override
    @SuppressLint("NewApi")
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        // Request Action
        String REQUEST = "CAMERA";

        // OnBackPress condition
        boolean onBackPress = false;

        // image uri
        Uri ImageURI = null;

        // image bitmap
        Bitmap imageBitmap = null;

        //Toast.makeText(this.getContext(), getString(requestCode), Toast.LENGTH_SHORT).show();
        //Log.d("requestCode", getString(requestCode));
        if(requestCode == CAMERA_SELECT_IMAGE_REQUEST_CODE){

            try {
                ImageURI = data.getData();
            } catch (Exception e) {
                Log.d("gallery error", e.toString());
                onBackPress = true;
            }

            REQUEST = "GALLERY";

        } else if(requestCode == CAMERA_CAPTURE_IMAGE_REQUEST_CODE){

            try {
                Bundle extras = data.getExtras();
                imageBitmap = (Bitmap) extras.get("data");
                getPath = "OK";
            } catch (Exception e) {
                Log.d("capture camera err", e.toString());
                onBackPress = true;
            }
        }

        // CONVERT LAYOUT WIDTH FROM IMAGE SELECT
        if (! onBackPress) {
            setImageFillParent(imageSelect);
        }

        // Preview data process
        try {
            if (requestCode == CAMERA_CAPTURE_IMAGE_REQUEST_CODE || requestCode == CAMERA_SELECT_IMAGE_REQUEST_CODE) {
                if (resultCode == Activity.RESULT_OK) {
                    switch (imageSelect.getId()) {
                        case R.id.image:

                            if (REQUEST.equals("CAMERA")) {
                                // from camera capture
                                image.setImageBitmap(imageBitmap);
                            } else {
                                // from gallery
                                image.setImageURI(ImageURI);
                            }

                            // set image64
                            image64 = toBase64(image);
                            // hide image upload text
                            imageText.setVisibility(View.GONE);
                            break;
                    }
                }
            }
        } catch (Exception e) {
            Log.d("Camera Error", e.toString());
        }
    }

    public void setImageFillParent(ImageView imageView) {
        imageView.getLayoutParams().width = ViewGroup.LayoutParams.FILL_PARENT;
        imageView.requestLayout();
    }

    public String toBase64 (ImageView image) {
        String base64 = "";
        try{
            BitmapDrawable drawable = (BitmapDrawable)image.getDrawable();
            Bitmap bitmap = drawable.getBitmap();
            bitmap = compressBitmap(bitmap);
            base64 = getImageBase64(bitmap);

        } catch (Exception e){
            try{
                image.buildDrawingCache();
                base64 = getImageBase64(image.getDrawingCache());
            } catch (Exception e1){
                Log.d("errorDrawing", e1.toString());
            }
            Log.d("errorBase64", e.toString());
        }
        return base64;
    }

    public static String getImageBase64(Bitmap bitmap)
    {
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 100, outputStream);

        return Base64.encodeToString(outputStream.toByteArray(), Base64.DEFAULT);
    }

    public Bitmap compressBitmap(Bitmap bitmap){
        int maxHeight = 500;
        int maxWidth = 500;
        float scale = Math.min(((float)maxHeight / bitmap.getWidth()), ((float)maxWidth / bitmap.getHeight()));

        Matrix matrix = new Matrix();
        matrix.postScale(scale, scale);
        bitmap = Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), matrix, true);
        return bitmap;
    }
}
