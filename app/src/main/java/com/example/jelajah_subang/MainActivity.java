package com.example.jelajah_subang;

import android.Manifest;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.ActionBar;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.support.v7.widget.SearchView;

import android.widget.LinearLayout;
import android.widget.Toast;

import com.android.volley.NetworkResponse;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.jelajah_subang.object.Kos;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.tasks.OnSuccessListener;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MainActivity extends MyCore implements OnMapReadyCallback,
        GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener,
        GoogleMap.OnMarkerDragListener,
        GoogleMap.OnMapLongClickListener,
        GoogleMap.OnMarkerClickListener{

    List<Kos> listData;
    SwipeRefreshLayout swLayout;
    RecyclePlace adapterList;
    ProgressDialog progressDialog;
    RecyclerView recycleList;
    int Home = 0;
    private SearchView searchView;

    // google map variable
    private static final String TAG = "MapsActivity";
    private GoogleMap mMap;
    private double longitude;
    private double latitude;
    private GoogleApiClient googleApiClient;
    LinearLayout mapLayout;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        ActionBar actionBar = getSupportActionBar();
        actionBar.setIcon(R.mipmap.ic_launcher);  // showing the icon to your action bar
        actionBar.setTitle(" Jelajah Subang");//set title to your action bar

        // Inisialisasi SwipeRefreshLayout
        swLayout = (SwipeRefreshLayout) findViewById(R.id.swlayout);

        listData = new ArrayList<Kos>();
        adapterList = new RecyclePlace(MainActivity.this, listData, false) {
            @Override
            public void setOnClickListener(Kos row) {
                //Toast.makeText(MainActivity.this, "Selected kos : " + row.getNama_kos(), Toast.LENGTH_SHORT).show();
                showDetailKos(row);
            }
        };

        // Mengeset properti warna yang berputar pada SwipeRefreshLayout
        swLayout.setColorSchemeResources(R.color.colorAccent,R.color.colorPrimary);

        // Mengeset listener yang akan dijalankan saat layar di refresh/swipe
        swLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {

                // Handler untuk menjalankan jeda selama 5 detik
                new Handler().postDelayed(new Runnable() {
                    @Override public void run() {

                        // Berhenti berputar/refreshing
                        swLayout.setRefreshing(false);

                        refreshData();
                    }
                }, 5000);
            }
        });

        recycleList = (RecyclerView) findViewById(R.id.recycleList);
        recycleList.setAdapter(adapterList);
        recycleList.setLayoutManager(new LinearLayoutManager(MainActivity.this));
        recycleList.setItemAnimator(new DefaultItemAnimator());

        refreshData();

        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
        mapLayout = (LinearLayout) findViewById(R.id.mapLayout);

        //Initializing googleApiClient
        googleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
    }

    @Override
    public void onBackPressed() {
        // close search view on back button pressed
        if (!searchView.isIconified()) {
            searchView.setIconified(true);
            return;
        }

        if(Home!=1){
            Home=1;
            Toast.makeText(this,"Please click BACK again to exit.", Toast.LENGTH_SHORT).show();

            new Handler().postDelayed(new Runnable() {

                @Override
                public void run() {
                    Home = 0;
                }
            }, 2000);
        } else {
            finish();
            moveTaskToBack(true);
            System.exit(0);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);

        // Associate searchable configuration with the SearchView
        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        searchView = (SearchView) menu.findItem(R.id.action_search)
                .getActionView();
        searchView.setSearchableInfo(searchManager
                .getSearchableInfo(getComponentName()));
        searchView.setMaxWidth(Integer.MAX_VALUE);
        searchView.setQueryHint("Cari berdasarkan nama wisata");

        // listening to search query text change
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                // filter recycler view when query submitted
                adapterList.getFilter().filter(query);
                return false;
            }

            @Override
            public boolean onQueryTextChange(String query) {
                //listData.clear();
                // filter recycler view when text is changed
                adapterList.getFilter().filter(query);
                return false;
            }
        });

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        return super.onOptionsItemSelected(item);
    }

    public void refreshData() {
        swLayout.setRefreshing(true);
        listData.clear();
        getServerData();
    }

    public void getServerData() {
        Log.d("Server URL", Public.SERVER_URL + "/jelajah_subang");
        StringRequest sReq = new StringRequest(com.android.volley.Request.Method.POST,
                Public.SERVER_URL + "/jelajah_subang",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        // close dialog
                        Log.d("response", response);
                        try {
                            JSONArray responses = new JSONArray(response);
                            Log.d("response object", response.toString());
                            final int numberOfItemsInResp = responses.length();
                            if(numberOfItemsInResp > 0){
                                for(int i = 0; i < numberOfItemsInResp; i++) {
                                    JSONObject data = responses.getJSONObject(i);

                                    Kos row = new Kos();
                                    row.setId_kos(data.getString("id"));
                                    row.setNama_kos(data.getString("nama_kos"));
                                    row.setId_vendor(data.getString("id_vendor"));
                                    row.setAlamat(data.getString("alamat"));
                                    row.setHarga(data.getString("harga"));
                                    row.setJumlah(data.getString("jumlah"));
                                    row.setDeskripsi(data.getString("deskripsi"));
                                    row.setFoto_kos(data.getString("foto_kos"));
                                    row.setSlider_kos(data.getString("slider_kos"));
                                    row.setEx_ac(data.getString("ex_ac"));
                                    row.setEx_kasur(data.getString("ex_kasur"));
                                    row.setEx_kipas(data.getString("ex_kipas"));
                                    row.setEx_lemari(data.getString("ex_lemari"));
                                    row.setEx_tv(data.getString("ex_tv"));
                                    row.setEx_wifi(data.getString("ex_wifi"));

                                    row.setLatitude(data.getString("latitude"));
                                    row.setLongitude(data.getString("longitude"));

                                    row.setTelepon(data.getString("telepon"));

                                    row.setJarak(data.getString("jarak"));

                                    listData.add(row);
                                }

                                adapterList.notifyDataSetChanged();

                            } else {
                                // get response data and convert to array
                                JSONObject data = responses.getJSONObject(0);

                                AlertDialog.Builder error = new AlertDialog.Builder(MainActivity.this);
                                error.setTitle("Connection Timeouttt");
                                error.setMessage(data.getString("message"));
                                error.create();
                                error.show();
                            }
                            swLayout.setRefreshing(false);
                        } catch (Exception e){
                            Log.d("error get data", e.toString());
                            swLayout.setRefreshing(false);
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        String jsonError = "Connection timeout";
                        NetworkResponse networkResponse = error.networkResponse;
                        if (networkResponse != null && networkResponse.data != null) {
                            jsonError = new String(networkResponse.data);
                            // Print Error!
                        }
                        Toast.makeText(MainActivity.this, error.toString(), Toast.LENGTH_SHORT).show();
                        swLayout.setRefreshing(false);
                    }
                }) {
            public Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("latitude", String.valueOf(latitude));
                params.put("longitude", String.valueOf(longitude));

                Log.d("params", params.toString());
                return params;
            }
        };
        Volley.newRequestQueue(MainActivity.this).add(sReq);
    }

    /** Called when the user taps the Send button */
    public void showDetailKos(Kos row) {
        Intent intent = new Intent(this, DetailPlaceActivity.class);

        intent.putExtra("id_kos", row.getId_kos());
        intent.putExtra("nama_kos", row.getNama_kos());
        intent.putExtra("foto_kos", row.getFoto_kos());
        intent.putExtra("slider_kos", row.getSlider_kos());
        intent.putExtra("id_vendor", row.getId_vendor());
        intent.putExtra("alamat", row.getAlamat());
        intent.putExtra("harga",  row.getHarga());
        intent.putExtra("jumlah", row.getJumlah());
        intent.putExtra("deskripsi", row.getDeskripsi());

        intent.putExtra("ex_ac", row.getEx_ac());
        intent.putExtra("ex_kasur", row.getEx_kasur());
        intent.putExtra("ex_kipas", row.getEx_kipas());
        intent.putExtra("ex_lemari", row.getEx_lemari());
        intent.putExtra("ex_tv", row.getEx_tv());
        intent.putExtra("ex_wifi", row.getEx_wifi());

        intent.putExtra("latitude", row.getLatitude());
        intent.putExtra("longitude", row.getLongitude());
        intent.putExtra("telepon", row.getTelepon());

        startActivity(intent);
    }

    // Google Map Function

    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        mMap.setMapType(GoogleMap.MAP_TYPE_HYBRID);
        // googleMapOptions.mapType(googleMap.MAP_TYPE_HYBRID)
        //    .compassEnabled(true);

        // Add a marker in Subang and move the camera
        latitude = -6.5601228;
        longitude = 107.7558147;
        LatLng current = new LatLng(latitude, longitude);
        mMap.addMarker(new MarkerOptions().position(current).title("Marker in Current Location"));
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(current, 13));
        mMap.setOnMarkerDragListener(this);
        mMap.setOnMapLongClickListener(this);

        googleMap.setMyLocationEnabled(true);
        googleMap.setOnMyLocationChangeListener(new GoogleMap.OnMyLocationChangeListener() {
            @Override
            public void onMyLocationChange(Location location) {

                latitude = location.getLatitude();
                longitude = location.getLongitude();
                LatLng current = new LatLng(latitude, longitude);
                mMap.addMarker(new MarkerOptions().position(current).title("Marker in Current Location"));
                mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(current, 13));
                mMap.setOnMarkerDragListener(MainActivity.this);
                mMap.setOnMapLongClickListener(MainActivity.this);

            }
        });

    }

    //Getting current location
    private void getCurrentLocation() {

        // GET CURRENT LOCATION
        FusedLocationProviderClient mFusedLocation = LocationServices.getFusedLocationProviderClient(this);
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        mFusedLocation.getLastLocation().addOnSuccessListener(this, new OnSuccessListener<Location>() {
            @Override
            public void onSuccess(Location location) {
                if (location != null) {
                    // Do it all with location
                    Log.d("My Current location", "Lat : " + location.getLatitude() + " Long : " + location.getLongitude());
                    // Display in Toast
                    longitude = location.getLatitude();
                    latitude = location.getLongitude();
                    // run action
                    moveMap();
                    // update list
                    getServerData();
                }
            }
        });
    }

    private void moveMap() {
        /**
         * Creating the latlng object to store lat, long coordinates
         * adding marker to map
         * move the camera with animation
         */
        mMap.clear();
        LatLng latLng = new LatLng(latitude, longitude);
        mMap.addMarker(new MarkerOptions()
                .position(latLng)
                .draggable(true)
                .title("Selected Place"));

        mMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));
        mMap.animateCamera(CameraUpdateFactory.zoomTo(15));
        mMap.getUiSettings().setZoomControlsEnabled(true);


    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        Toast.makeText(MainActivity.this, "Location changed", Toast.LENGTH_SHORT).show();
        getCurrentLocation();
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onMapLongClick(LatLng latLng) {
        // mMap.clear();
        mMap.addMarker(new MarkerOptions().position(latLng).draggable(true));
    }

    @Override
    public void onMarkerDragStart(Marker marker) {
        Toast.makeText(MainActivity.this, "onMarkerDragStart", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onMarkerDrag(Marker marker) {
        Toast.makeText(MainActivity.this, "onMarkerDrag", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onMarkerDragEnd(Marker marker) {
        // getting the Co-ordinates
        latitude = marker.getPosition().latitude;
        longitude = marker.getPosition().longitude;

        //move to current position
        moveMap();
    }

    @Override
    protected void onStart() {
        //googleApiClient.connect();
        super.onStart();
    }

    @Override
    protected void onStop() {
        //googleApiClient.disconnect();
        super.onStop();
    }


    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    @Override
    public boolean onMarkerClick(Marker marker) {
        return false;
    }

}
