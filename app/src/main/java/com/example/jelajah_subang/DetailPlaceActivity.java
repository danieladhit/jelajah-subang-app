package com.example.jelajah_subang;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.Location;
import android.net.Uri;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.v4.app.ActivityCompat;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.jelajah_subang.network.ApiServices;
import com.example.jelajah_subang.network.InitLibrary;
import com.example.jelajah_subang.response.Distance;
import com.example.jelajah_subang.response.Duration;
import com.example.jelajah_subang.response.LegsItem;
import com.example.jelajah_subang.response.ResponseRoute;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.maps.android.PolyUtil;
import com.squareup.picasso.Picasso;
import com.synnapps.carouselview.CarouselView;
import com.synnapps.carouselview.ImageListener;

import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.example.jelajah_subang.Public.BASE_URL;

public class DetailPlaceActivity extends MyCore implements View.OnClickListener {

    CarouselView carouselView;
    TextView tvNama, tvAlamat, tvKeterangan, tvDeskripsi, tvJarak;
    android.support.v7.widget.GridLayout gridFitur;
    ImageView imageGrid1, imageGrid2, imageGrid3, imageGrid4, imageGrid5, imageGrid6;
    LinearLayout boxGrid1, boxGrid2, boxGrid3, boxGrid4, boxGrid5, boxGrid6;
    Button btnTelepon, btnPetunjuk, btnAgenda;

    int[] sampleImages = {R.drawable.kos1, R.drawable.kos2, R.drawable.kos3, R.drawable.kos4, R.drawable.kos5};
    int blankImage = R.drawable.img_blank_subang;
    int notFoundImage = R.drawable.img_not_found;
    String[] sampleNetworkImageURLs = {
            "https://via.placeholder.com/350x150.png?text=image1",
            "https://via.placeholder.com/350x150.png?text=image2",
            "https://via.placeholder.com/350x150.png?text=image3",
            "https://via.placeholder.com/350x150.png?text=image4",
            "https://via.placeholder.com/350x150.png?text=image5"
    };

    String id_kos = "", nama_kos = "", id_vendor = "", alamat = "", harga = "", jumlah = "", deskripsi = "", ex_kasur = "", ex_lemari = "", ex_ac = "", ex_kipas = "", ex_wifi = "", ex_tv = "";
    String foto_kos = "", slider_kos = "";

    private String longitude;
    private String latitude;
    String telepon;

    private String API_KEY = "AIzaSyBjvNqYLw5DEpwqree8gJylsXofzR64ZY4";

    private LatLng pickUpLatLng = new LatLng(-6.2293867,106.6894306); // Jakarta
    private LatLng locationLatLng = new LatLng(-6.5601228, 107.7558147); // Subang
    private double currentLatitude;
    private double currentLongitude;
    private GoogleApiClient googleApiClient;
    private static final int LOCATION = 1;


    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_kos);

        // GET BUNDLE EXTRA DATA from previous activity
        getExtrasData();

        tvNama = (TextView) findViewById(R.id.tvNama);
        tvAlamat = (TextView) findViewById(R.id.tvAlamat);
        tvKeterangan = (TextView) findViewById(R.id.tvKeterangan);
        tvJarak = (TextView) findViewById(R.id.tvJarak);
        tvDeskripsi = (TextView) findViewById(R.id.tvDeskripsi);
        btnTelepon = (Button) findViewById(R.id.btnTelepon);
        btnTelepon.setOnClickListener(this);
        btnPetunjuk = (Button) findViewById(R.id.btnPetunjuk);
        btnPetunjuk.setOnClickListener(this);
        btnAgenda = (Button) findViewById(R.id.btnAgenda);
        btnAgenda.setOnClickListener(this);

        tvNama.setText(nama_kos);
        tvAlamat.setText(alamat);
        tvKeterangan.setText(harga);
        tvDeskripsi.setText(deskripsi);
        tvJarak.setText("menghitung jarak lokasi...");

        sampleNetworkImageURLs = explodeStringUsingStringUtils(slider_kos, ",");
        carouselView = (CarouselView) findViewById(R.id.carouselView);
        carouselView.setPageCount(sampleNetworkImageURLs.length);
        carouselView.setImageListener(imageListener);

        getCurrentLocation();
    }

    public void getExtrasData() {
        Bundle bundle = getIntent().getExtras();

        id_kos = bundle.getString("id_kos");
        nama_kos = bundle.getString("nama_kos");
        foto_kos = bundle.getString("foto_kos");
        slider_kos = bundle.getString("slider_kos");
        id_vendor = bundle.getString("id_vendor");
        alamat = bundle.getString("alamat");
        harga = bundle.getString("harga");
        jumlah = bundle.getString("jumlah");
        deskripsi = bundle.getString("deskripsi");

        ex_ac = bundle.getString("ex_ac");
        ex_kasur = bundle.getString("ex_kasur");
        ex_kipas = bundle.getString("ex_kipas");
        ex_lemari = bundle.getString("ex_lemari");
        ex_tv = bundle.getString("ex_tv");
        ex_wifi = bundle.getString("ex_wifi");

        latitude = bundle.getString("latitude");
        longitude = bundle.getString("longitude");
        telepon = bundle.getString("telepon");
    }

    private String[] explodeStringUsingStringUtils(String stringToExplode, String separator) {
        return StringUtils.splitPreserveAllTokens(stringToExplode, separator);
    }

    // To set simple images
    ImageListener imageListener = new ImageListener() {
        @Override
        public void setImageForPosition(int position, ImageView imageView) {
            String firstFoto = BASE_URL + sampleNetworkImageURLs[0].trim();
            String sliderFoto = BASE_URL + sampleNetworkImageURLs[position].trim();
            Log.d("slider_foto", sliderFoto);
            Picasso.get().load(sliderFoto).placeholder(blankImage).error(notFoundImage).fit().centerCrop().into(imageView);

            //imageView.setImageResource(sampleImages[position]);
        }
    };

    public void  call_action() {
        Intent i = new Intent(Intent.ACTION_CALL);
        i.setData(Uri.parse("tel:"+telepon));
        startActivity(i);
    }

    @Override
    public void onClick(View v) {
        Intent intent = null;
        switch (v.getId()) {

            case R.id.btnTelepon:
                // TODO Auto-generated method stub
                if (isPermissionGranted()) {
                    call_action();
                }
                break;

            case R.id.btnPetunjuk:

                intent = new Intent(this, DirectionActivity.class);
                intent.putExtra("id_kos", id_kos);
                intent.putExtra("nama_kos", nama_kos);
                intent.putExtra("foto_kos", foto_kos);
                intent.putExtra("slider_kos", slider_kos);
                intent.putExtra("id_vendor", id_vendor);
                intent.putExtra("alamat", alamat);
                intent.putExtra("harga", harga);
                intent.putExtra("jumlah", jumlah);
                intent.putExtra("deskripsi", deskripsi);

                intent.putExtra("latitude", latitude);
                intent.putExtra("longitude", longitude);
                intent.putExtra("telepon", telepon);

                startActivity(intent);
                break;

            case R.id.btnAgenda:
                if (isLogin()) {
                    // checkout process
                    intent = new Intent(this, CheckoutActivity.class);
                    intent.putExtra("email", SP_EMAIL);
                } else {
                    // go to login page
                    setLoginRedirectActivity(ACTIVITY_CHECKOUT);
                    intent = new Intent(this, LoginActivity.class);
                }

                intent.putExtra("id_kos", id_kos);
                intent.putExtra("nama_kos", nama_kos);
                intent.putExtra("foto_kos", foto_kos);
                intent.putExtra("slider_kos", slider_kos);
                intent.putExtra("id_vendor", id_vendor);
                intent.putExtra("alamat", alamat);
                intent.putExtra("harga", harga);
                intent.putExtra("jumlah", jumlah);
                intent.putExtra("deskripsi", deskripsi);

                startActivity(intent);

                break;

            default:
                break;
        }
    }

    //Getting current location
    private void getCurrentLocation() {
        //mMap.clear();

        // GET CURRENT LOCATION
        FusedLocationProviderClient mFusedLocation = LocationServices.getFusedLocationProviderClient(this);
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        mFusedLocation.getLastLocation().addOnSuccessListener(this, new OnSuccessListener<Location>() {
            @Override
            public void onSuccess(Location location) {
                if (location != null) {
                    // Do it all with location
                    Log.d("My Current location", "Lat : " + location.getLatitude() + " Long : " + location.getLongitude());
                    // Display in Toast
                    currentLatitude = location.getLatitude();
                    currentLongitude = location.getLongitude();
                    // run action
                    actionRoute();
                }
            }
        });

    }

    private void actionRoute() {
        /*String lokasiAwal = pickUpLatLng.latitude + "," + pickUpLatLng.longitude;
        String lokasiAkhir = locationLatLng.latitude + "," + locationLatLng.longitude;*/

        // Start Location
        String lokasiAwal = currentLatitude + "," + currentLongitude;
        pickUpLatLng = new LatLng(currentLatitude, currentLongitude);

        // End Location
        String lokasiAkhir = latitude + "," + longitude;
        locationLatLng = new LatLng(Double.valueOf(latitude), Double.valueOf(longitude));
        Log.d("locationnnn", lokasiAwal.toString() + " -- " + lokasiAkhir.toString());
        // Panggil Retrofit
        ApiServices api = InitLibrary.getInstance();
        // Siapkan request
        Call<ResponseRoute> routeRequest = api.request_route(lokasiAwal, lokasiAkhir, API_KEY);
        // kirim request
        routeRequest.enqueue(new Callback<ResponseRoute>() {
            @Override
            public void onResponse(Call<ResponseRoute> call, Response<ResponseRoute> response) {

                if (response.isSuccessful()) {
                    // tampung response ke variable
                    ResponseRoute dataDirection = response.body();
                    Log.d("Map Response", dataDirection.toString());

                    LegsItem dataLegs = dataDirection.getRoutes().get(0).getLegs().get(0);


                    // Dapatkan jarak dan waktu
                    Distance dataDistance = dataLegs.getDistance();
                    Duration dataDuration = dataLegs.getDuration();

                    //tvDistance.setText("Jarak : " + dataDistance.getText());
                    tvJarak.setText("Waktu Tempuh : " + dataDuration.getText());

                }
            }

            @Override
            public void onFailure(Call<ResponseRoute> call, Throwable t) {
                t.printStackTrace();
            }
        });
    }


    @Override
    public void onBackPressed() {
        /*// close search view on back button pressed
        if (!searchView.isIconified()) {
            searchView.setIconified(true);
            return;
        }*/

        Intent intent = new Intent(this, MainActivity.class);

        startActivity(intent);
    }

    public  boolean isPermissionGranted() {
        if (Build.VERSION.SDK_INT >= 23) {
            if (checkSelfPermission(android.Manifest.permission.CALL_PHONE)
                    == PackageManager.PERMISSION_GRANTED) {
                Log.v("TAG","Permission is granted");
                return true;
            } else {

                Log.v("TAG","Permission is revoked");
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.CALL_PHONE}, 1);
                return false;
            }
        }
        else { //permission is automatically granted on sdk<23 upon installation
            Log.v("TAG","Permission is granted");
            return true;
        }
    }


    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {

            case 1: {

                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Toast.makeText(getApplicationContext(), "Permission granted", Toast.LENGTH_SHORT).show();
                    call_action();
                } else {
                    //Toast.makeText(getApplicationContext(), "Permission denied", Toast.LENGTH_SHORT).show();
                }
                return;
            }

            // other 'case' lines to check for other
            // permissions this app might request
        }
    }
}
