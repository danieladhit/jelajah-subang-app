package com.example.jelajah_subang.object;

/**
 * Created by jhedhot 2018
 */

public class Kos {
    String id_kos="", nama_kos="", id_vendor="", alamat="", harga="", jumlah="", deskripsi="", ex_kasur="", ex_lemari="", ex_ac="", ex_kipas="", ex_wifi="", ex_tv="";
    String nama_vendor="", telepon_vendor="", email_vendor="", alamat_vendor="";
    String foto_kos="", slider_kos="";
    String latitude="", longitude="";
    String telepon="";
    String jarak="";

    // data order
    String order_id="",customer="", periode="", waktu_order="", harga_sewa="", no_rekening="", jumlah_transfer="", bukti_pembayaran="", status_order="";

    public String getId_kos() {
        return id_kos;
    }

    public void setId_kos(String id_kos) {
        this.id_kos = id_kos;
    }

    public String getNama_kos() {
        return nama_kos;
    }

    public void setNama_kos(String nama_kos) {
        this.nama_kos = nama_kos;
    }

    public String getId_vendor() {
        return id_vendor;
    }

    public void setId_vendor(String id_vendor) {
        this.id_vendor = id_vendor;
    }

    public String getAlamat() {
        return alamat;
    }

    public void setAlamat(String alamat) {
        this.alamat = alamat;
    }

    public String getHarga() {
        return harga;
    }

    public void setHarga(String harga) {
        this.harga = harga;
    }

    public String getJumlah() {
        return jumlah;
    }

    public void setJumlah(String jumlah) {
        this.jumlah = jumlah;
    }

    public String getDeskripsi() {
        return deskripsi;
    }

    public void setDeskripsi(String deskripsi) {
        this.deskripsi = deskripsi;
    }

    public String getEx_kasur() {
        return ex_kasur;
    }

    public void setEx_kasur(String ex_kasur) {
        this.ex_kasur = ex_kasur;
    }

    public String getEx_lemari() {
        return ex_lemari;
    }

    public void setEx_lemari(String ex_lemari) {
        this.ex_lemari = ex_lemari;
    }

    public String getEx_ac() {
        return ex_ac;
    }

    public void setEx_ac(String ex_ac) {
        this.ex_ac = ex_ac;
    }

    public String getEx_kipas() {
        return ex_kipas;
    }

    public void setEx_kipas(String ex_kipas) {
        this.ex_kipas = ex_kipas;
    }

    public String getEx_wifi() {
        return ex_wifi;
    }

    public void setEx_wifi(String ex_wifi) {
        this.ex_wifi = ex_wifi;
    }

    public String getEx_tv() {
        return ex_tv;
    }

    public void setEx_tv(String ex_tv) {
        this.ex_tv = ex_tv;
    }

    public String getNama_vendor() {
        return nama_vendor;
    }

    public void setNama_vendor(String nama_vendor) {
        this.nama_vendor = nama_vendor;
    }

    public String getTelepon_vendor() {
        return telepon_vendor;
    }

    public void setTelepon_vendor(String telepon_vendor) {
        this.telepon_vendor = telepon_vendor;
    }

    public String getEmail_vendor() {
        return email_vendor;
    }

    public void setEmail_vendor(String email_vendor) {
        this.email_vendor = email_vendor;
    }

    public String getAlamat_vendor() {
        return alamat_vendor;
    }

    public void setAlamat_vendor(String alamat_vendor) {
        this.alamat_vendor = alamat_vendor;
    }

    public String getFoto_kos() {
        return foto_kos;
    }

    public void setFoto_kos(String foto_kos) {
        this.foto_kos = foto_kos;
    }

    public String getSlider_kos() {
        return slider_kos;
    }

    public void setSlider_kos(String slider_kos) {
        this.slider_kos = slider_kos;
    }

    public String getOrder_id() {
        return order_id;
    }

    public void setOrder_id(String order_id) {
        this.order_id = order_id;
    }

    public String getCustomer() {
        return customer;
    }

    public void setCustomer(String customer) {
        this.customer = customer;
    }

    public String getPeriode() {
        return periode;
    }

    public void setPeriode(String periode) {
        this.periode = periode;
    }

    public String getWaktu_order() {
        return waktu_order;
    }

    public void setWaktu_order(String waktu_order) {
        this.waktu_order = waktu_order;
    }

    public String getHarga_sewa() {
        return harga_sewa;
    }

    public void setHarga_sewa(String harga_sewa) {
        this.harga_sewa = harga_sewa;
    }

    public String getNo_rekening() {
        return no_rekening;
    }

    public void setNo_rekening(String no_rekening) {
        this.no_rekening = no_rekening;
    }

    public String getJumlah_transfer() {
        return jumlah_transfer;
    }

    public void setJumlah_transfer(String jumlah_transfer) {
        this.jumlah_transfer = jumlah_transfer;
    }

    public String getBukti_pembayaran() {
        return bukti_pembayaran;
    }

    public void setBukti_pembayaran(String bukti_pembayaran) {
        this.bukti_pembayaran = bukti_pembayaran;
    }

    public String getStatus_order() {
        return status_order;
    }

    public void setStatus_order(String status_order) {
        this.status_order = status_order;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getTelepon() {
        return telepon;
    }

    public void setTelepon(String telepon) {
        this.telepon = telepon;
    }

    public String getJarak() {
        return jarak;
    }

    public void setJarak(String jarak) {
        this.jarak = jarak;
    }
}
