package com.example.jelajah_subang;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.TextView;
import android.widget.Toast;

import com.example.jelajah_subang.network.ApiServices;
import com.example.jelajah_subang.network.InitLibrary;
import com.example.jelajah_subang.response.Distance;
import com.example.jelajah_subang.response.Duration;
import com.example.jelajah_subang.response.LegsItem;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.maps.android.PolyUtil;

import com.example.jelajah_subang.response.ResponseRoute;

import java.util.List;

import pub.devrel.easypermissions.AfterPermissionGranted;
import pub.devrel.easypermissions.EasyPermissions;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DirectionActivity extends AppCompatActivity implements OnMapReadyCallback,
        GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener,
        EasyPermissions.PermissionCallbacks {
    private GoogleMap mMap;

    private String API_KEY = "AIzaSyBjvNqYLw5DEpwqree8gJylsXofzR64ZY4";

    private LatLng pickUpLatLng = new LatLng(-6.2293867,106.6894306); // Jakarta
    private LatLng locationLatLng = new LatLng(-6.5601228, 107.7558147); // Subang

    private TextView tvStartAddress, tvEndAddress, tvDuration, tvDistance;

    String id_kos = "", nama_kos = "", id_vendor = "", alamat = "", harga = "", jumlah = "", deskripsi = "", ex_kasur = "", ex_lemari = "", ex_ac = "", ex_kipas = "", ex_wifi = "", ex_tv = "";
    String foto_kos = "", slider_kos = "";

    private String longitude;
    private String latitude;
    String telepon;

    private double currentLatitude;
    private double currentLongitude;
    private GoogleApiClient googleApiClient;
    private static final int LOCATION = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_direction);
        // GET BUNDLE EXTRA DATA from previous activity
        getExtrasData();

        // Here, thisActivity is the current activity
        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {

            // Permission is not granted
            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    Manifest.permission.ACCESS_FINE_LOCATION)) {
                Toast.makeText(this, "Membutuhkan Izin Lokasi", Toast.LENGTH_SHORT).show();
            } else {

                // No explanation needed; request the permission
                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION},
                        1);
            }
        } else {
            // Permission has already been granted
            Toast.makeText(this, "Izin Lokasi diberikan", Toast.LENGTH_SHORT).show();
        }

        // Maps
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
        //Initializing googleApiClient
        googleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();

        // Set Title bar
        getSupportActionBar().setTitle("Petunjuk Arah");
        // Inisialisasi Widget
        widgetInit();
        // jalankan method
        //actionRoute();
        getCurrentLocation();
    }

    public void getExtrasData() {
        Bundle bundle = getIntent().getExtras();

        id_kos = bundle.getString("id_kos");
        nama_kos = bundle.getString("nama_kos");
        foto_kos = bundle.getString("foto_kos");
        slider_kos = bundle.getString("slider_kos");
        id_vendor = bundle.getString("id_vendor");
        alamat = bundle.getString("alamat");
        harga = bundle.getString("harga");
        jumlah = bundle.getString("jumlah");
        deskripsi = bundle.getString("deskripsi");

        ex_ac = bundle.getString("ex_ac");
        ex_kasur = bundle.getString("ex_kasur");
        ex_kipas = bundle.getString("ex_kipas");
        ex_lemari = bundle.getString("ex_lemari");
        ex_tv = bundle.getString("ex_tv");
        ex_wifi = bundle.getString("ex_wifi");

        latitude = bundle.getString("latitude");
        longitude = bundle.getString("longitude");
        telepon = bundle.getString("telepon");
    }

    private void widgetInit() {
        currentLatitude = pickUpLatLng.latitude;
        currentLongitude = pickUpLatLng.longitude;

        tvStartAddress = findViewById(R.id.tvStartAddress);
        tvEndAddress = findViewById(R.id.tvEndAddress);
        tvDuration = findViewById(R.id.tvDuration);
        tvDistance = findViewById(R.id.tvDistance);
    }

    private void actionRoute() {
        /*String lokasiAwal = pickUpLatLng.latitude + "," + pickUpLatLng.longitude;
        String lokasiAkhir = locationLatLng.latitude + "," + locationLatLng.longitude;*/

        // Start Location
        String lokasiAwal = currentLatitude + "," + currentLongitude;
        pickUpLatLng = new LatLng(currentLatitude, currentLongitude);

        // End Location
        String lokasiAkhir = latitude + "," + longitude;
        locationLatLng = new LatLng(Double.valueOf(latitude), Double.valueOf(longitude));

        // Panggil Retrofit
        ApiServices api = InitLibrary.getInstance();
        // Siapkan request
        Call<ResponseRoute> routeRequest = api.request_route(lokasiAwal, lokasiAkhir, API_KEY);
        // kirim request
        routeRequest.enqueue(new Callback<ResponseRoute>() {
            @Override
            public void onResponse(Call<ResponseRoute> call, Response<ResponseRoute> response) {

                if (response.isSuccessful()) {
                    // tampung response ke variable
                    ResponseRoute dataDirection = response.body();

                    LegsItem dataLegs = dataDirection.getRoutes().get(0).getLegs().get(0);

                    // Dapatkan garis polyline
                    String polylinePoint = dataDirection.getRoutes().get(0).getOverviewPolyline().getPoints();
                    // Decode
                    List<LatLng> decodePath = PolyUtil.decode(polylinePoint);
                    // Gambar garis ke maps
                    mMap.addPolyline(new PolylineOptions().addAll(decodePath)
                            .width(8f).color(Color.argb(255, 56, 167, 252)))
                            .setGeodesic(true);

                    // Tambah Marker
                    mMap.addMarker(new MarkerOptions().position(pickUpLatLng).title("Lokasi Anda"));
                    mMap.addMarker(new MarkerOptions().position(locationLatLng).title("Lokasi Tujuan"));
                    // Dapatkan jarak dan waktu
                    Distance dataDistance = dataLegs.getDistance();
                    Duration dataDuration = dataLegs.getDuration();

                    // Set Nilai Ke Widget
                    tvStartAddress.setText("Lokasi Anda : " + dataLegs.getStartAddress().toString());
                    tvEndAddress.setText("Lokasi Tujuan : " + dataLegs.getEndAddress().toString());

                    tvDistance.setText("Jarak : " + dataDistance.getText());
                    tvDuration.setText("Waktu Tempuh : " + dataDuration.getText());
                    /** START
                     * Logic untuk membuat layar berada ditengah2 dua koordinat
                     */

                    LatLngBounds.Builder latLongBuilder = new LatLngBounds.Builder();
                    latLongBuilder.include(pickUpLatLng);
                    latLongBuilder.include(locationLatLng);

                    // Bounds Coordinata
                    LatLngBounds bounds = latLongBuilder.build();

                    int width = getResources().getDisplayMetrics().widthPixels;
                    int height = getResources().getDisplayMetrics().heightPixels;
                    int paddingMap = (int) (width * 0.2); //jarak dari
                    CameraUpdate cu = CameraUpdateFactory.newLatLngBounds(bounds, width, height, paddingMap);
                    mMap.animateCamera(cu);

                    /** END
                     * Logic untuk membuat layar berada ditengah2 dua koordinat
                     */

                }
            }

            @Override
            public void onFailure(Call<ResponseRoute> call, Throwable t) {
                t.printStackTrace();
            }
        });
    }


    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
    }

    //Getting current location
    private void getCurrentLocation() {
        //mMap.clear();

        // GET CURRENT LOCATION
        FusedLocationProviderClient mFusedLocation = LocationServices.getFusedLocationProviderClient(this);
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        mFusedLocation.getLastLocation().addOnSuccessListener(this, new OnSuccessListener<Location>() {
            @Override
            public void onSuccess(Location location) {
                if (location != null) {
                    // Do it all with location
                    Log.d("My Current location", "Lat : " + location.getLatitude() + " Long : " + location.getLongitude());
                    // Display in Toast
                    currentLatitude = location.getLatitude();
                    currentLongitude = location.getLongitude();
                    // run action
                    mMap.clear();
                    actionRoute();
                }
            }
        });

    }


    @Override
    public void onConnected(@Nullable Bundle bundle) {
        getCurrentLocation();
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onPointerCaptureChanged(boolean hasCapture) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }


    // easy permission implements method
    @AfterPermissionGranted(LOCATION)
    private void methodRequiresTwoPermission() {
        String[] perms = {Manifest.permission.ACCESS_FINE_LOCATION};
        if (EasyPermissions.hasPermissions(this, perms)) {
            // Already have permission, do the thing
            Toast.makeText(this, "permission ok", Toast.LENGTH_SHORT).show();
        } else {
            // Do not have permissions, request them now
            EasyPermissions.requestPermissions(this, "Izinkan akses lokasi",
                    LOCATION, perms);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        // Forward results to EasyPermissions
        EasyPermissions.onRequestPermissionsResult(requestCode, permissions, grantResults, this);
    }

    @Override
    public void onPermissionsGranted(int requestCode, @NonNull List<String> perms) {

    }

    @Override
    public void onPermissionsDenied(int requestCode, @NonNull List<String> perms) {

    }
}
